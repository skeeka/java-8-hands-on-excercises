package com.hybris.java8dojo;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Sets;

public class StreamsTest1 {

    private List<String> mob;
    private Stream<String> stream;

    /*
     * Collection = DVD (contains whole data structure) - compute, then populate
     * Stream = Streaming media from Internet (stream of bytes) - compute on demand
     */

    @Before
    public void setUp()
    {
	mob = asList("wallaby", "red", "grey", "boomer", "pademelon");
	stream = null;
	//stream = Stream.of("red", "grey");
    }

    /*
     * 1. External vs Internal iteration.
     *
     * External = instructions -> control blocks + hard to parallelize
     * Internal = declarative -> SQL-like querying DSL + easy to parallelize (no mutable state + framework can optimize at will)
     *
     * Put toys away in a box.
     * External: Go get the ball, put the ball in the box. Anything else? Go get the doll, put the doll in the box.... Anything else? No, OK all done.
     * Internal: Put all toys in the box.
     */
    @Test
    public void externalIteration_Filter()
    {
	List<String> namesWithAnE = new ArrayList<>();

	for (String string : mob) {
	    if (string.contains("e"))
	    {
		namesWithAnE.add(string);
	    }
	}

	Assert.assertEquals(Arrays.asList("red", "grey", "boomer", "pademelon"), namesWithAnE);
    }

    @Test
    public void internalIteration_SequentialFilter()
    {
	List<String> namesWithAnE = mob.stream().filter(name -> name.contains("e")).collect(Collectors.toList());

	Assert.assertEquals(Arrays.asList("red", "grey", "boomer", "pademelon"), namesWithAnE);
    }

    @Test
    public void internalIteration_ParallelFilter()
    {
	List<String> namesWithAnE = mob.parallelStream().filter(name -> name.contains("e")).collect(Collectors.toList());

	Assert.assertEquals(Arrays.asList("red", "grey", "boomer", "pademelon"), namesWithAnE);
    }


    /*
     * Working with Streams.
     *
     * Intermediate Operations
     * 		- Returns a stream
     * 		- Pipeline of operations
     * 		- Lazy - only executed when a terminal operator is called
     *
     * Terminal Operations
     * 		- Returns non-stream
     * 		- Closes the stream (cannot be processed again)
     *
     * Create Stream --> Apply Intermediate Operator  --> Apply Terminal Operator
     * 		     |				      |
     * 		     |				      |
     * 		     |________________________________|
     */


    /*
     * Basic Creating Streams.
     *
     * 1. Stream.of(T t)
     *
     * 2. Arrays.stream(T[])
     *
     * 3. list/set .stream()
     *
     */
    @Test
    public void createStream_of()
    {

	Assert.assertEquals(2, stream.count());
    }

    @Test
    public void createStream_Arrays()
    {

	Assert.assertEquals(2, stream.count());
    }

    @Test
    public void createStream_List()
    {

	Assert.assertEquals(2, stream.count());
    }

    @Test
    public void createStream_Set()
    {

	Assert.assertEquals(2, stream.count());
    }

    /*
     * Basic Terminal Operators
     *
     * Terminal Operators: count, forEach, toArray, collect
     */
    @Test
    public void printItemsInStream_ForEach()
    {

    }

    @Test
    public void addItemsToList_ForEach()
    {
	List<String> names = null;
	Assert.assertEquals(Arrays.asList("red", "grey"), names);
    }

    @Test
    @SuppressWarnings("deprecation")
    public void collectItems_toArray()
    {
	Object[] names = null;
	Assert.assertEquals(new String[] {"red", "grey"}, names);
    }

    @Test
    public void collectItemsAsList_Collect()
    {

	List<String> names = null;
	Assert.assertEquals(Arrays.asList("red", "grey"), names);
    }

    @Test
    public void collectItemsAsSet_Collect()
    {

	Set<String> names = null;
	Assert.assertEquals(Sets.newHashSet(Arrays.asList("red", "grey")), names);
    }
}