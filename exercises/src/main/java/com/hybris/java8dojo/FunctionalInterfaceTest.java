package com.hybris.java8dojo;

import org.junit.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by i840081 on 2015-07-03.
 */
public class FunctionalInterfaceTest {

    /*
        1. Functional Interfaces

        A functional interface is defined as any interface that has exactly "one" explicitly declared abstract method
            - The qualification is necessary because an interface may have non-abstract default methods.

        2. Anonymous Inner Classes

        Anonymous Inner Classes enable you to declare and instantiate a class at the same time.
        They are like local classes except that they do not have a name.
            - Use them if you need to use a local class only once.

        3. Java uses FI all the time. (Collections)

        4. When you see a Functional Interface, write a Lambda instead

        5. Lambdas are just shortcuts for Anonymous Inner Classes

        https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html

     */


    @FunctionalInterface
    public interface KangarooJumpInterface
    {
        Integer calculateJumpForce();
    }

    @Test
    public void testJumpingKangaroo()
    {
        KangarooJumpInterface kangarooInterface = new KangarooJumpInterface() {

            @Override
            public Integer calculateJumpForce() {
                return 10;
            }
        };
        assertThat(kangarooInterface.calculateJumpForce(), is(10));
    }

    @Test
    public void sortKangaroos()
    {
        List<String> mob = asList("wallaby", "red", "grey", "boomer", "pademelon");
        Collections.sort(mob, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        assertThat(mob, is(asList("boomer", "grey", "pademelon", "red", "wallaby")));
    }

    @Test
    public void sortKangaroosAlmostCool()
    {
        List<String> mob = asList("wallaby", "red", "grey", "boomer", "pademelon");
        Collections.sort(mob, (String o1, String o2) -> {
            return o1.compareTo(o2);
        });

        assertThat(mob, is(asList("boomer", "grey", "pademelon", "red", "wallaby")));
    }

    @Test
    public void sortKangaroosCool()
    {
        List<String> mob = asList("wallaby", "red", "grey", "boomer", "pademelon");
        Collections.sort(mob, (o1, o2) -> o1.compareTo(o2));

        assertThat(mob, is(asList("boomer", "grey", "pademelon", "red", "wallaby")));
    }
}
