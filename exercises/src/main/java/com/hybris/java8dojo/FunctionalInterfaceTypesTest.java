package com.hybris.java8dojo;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created by i840081 on 2015-07-03.
 */
public class FunctionalInterfaceTypesTest {

    /*
        1. Functional Interface Types
         a. Supplier
         b. Consumer
         c. Function
         d. Predicate

     */

    Supplier s = new Supplier() {
        @Override
        public Object get() {
            return null;
        }
    };
    Supplier s1 = () -> null;


    Consumer c = new Consumer() {
        @Override
        public void accept(Object o) {

        }
    };
    Consumer c1 = (o) -> System.out.print(0);

    Function f = new Function() {
        @Override
        public Object apply(Object o) {
            return null;
        }
    };
    Function<String, Boolean> f1 = (String o) -> false;

    Predicate p = new Predicate() {
        @Override
        public boolean test(Object o) {
            return false;
        }
    };
    Predicate p1 = (o) -> false;
}
