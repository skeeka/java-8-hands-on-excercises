package com.hybris.java8dojo.solutions;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.xebia.domain.Person;
import com.xebia.domain.Person.Role;

public class StreamsTest2Solution {

    private Person alice;
    private Person bob;
    private Person charlie;
    private Stream<Person> stream;

    @Before
    public void setUp() throws Exception {
        alice = new Person("Alice", 31, Boolean.FALSE, Role.PROGRAMMER, "Video Games", "Running");
        bob = new Person("Bob", 12, Boolean.TRUE, Role.PROGRAMMER, "Video Games");
        charlie = new Person("Charlie", 37, Boolean.TRUE, Role.TESTER, "Video Games", "Running", "Cooking");
        stream = Stream.of(alice, bob, charlie);
    }

    /*
     * Filtering and Slicing
     *
     * Intermediate Operators:
     * 		- filter(Predicate p)
     * 		- sorted(Comparator c)
     * 		- distinct()
     * 		- limit(long n)
     * 		- skip(long n)
     */
    @Test
    public void shouldFilterByNameEquals()
    {
	List<Person> persons = stream.filter(person -> "Alice".equals(person.getName())).collect(toList());

	Assert.assertEquals("Alice", persons.iterator().next().getName());
    }

    @Test
    public void shouldFilterByAgeGreaterThan()
    {
	List<Person> persons = stream.filter(person -> person.getAge() > 21).collect(toList());

	Assert.assertEquals(2, persons.size());
	Assert.assertTrue(persons.stream().anyMatch(person -> "Alice".equals(person.getName())));
	Assert.assertTrue(persons.stream().anyMatch(person -> "Charlie".equals(person.getName())));
    }

    @Test
    public void shouldFilterByHobbie_Running()
    {
	List<Person> persons = stream.filter(person -> person.getHobbies().contains("Running")).collect(toList());

	Assert.assertEquals(2, persons.size());
	Assert.assertTrue(persons.stream().anyMatch(person -> "Alice".equals(person.getName())));
	Assert.assertTrue(persons.stream().anyMatch(person -> "Charlie".equals(person.getName())));
    }

    @Test
    public void shouldFilterBySexAndRole()
    {
	List<Person> persons = stream.filter(Person::isMale).filter(person -> person.getRole().equals(Role.PROGRAMMER)).collect(toList());

	Assert.assertEquals(1, persons.size());
	Assert.assertTrue(persons.stream().anyMatch(person -> "Bob".equals(person.getName())));
    }

    @Test
    public void shouldSortByAge()
    {
	//List<Person> persons = stream.sorted((person1, person2) -> person1.getAge() - person2.getAge()).collect(toList());
	//List<Person> persons = stream.sorted(Comparator.comparing(person -> person.getAge())).collect(toList());
	List<Person> persons = stream.sorted(Comparator.comparing(Person::getAge)).collect(toList());

	Assert.assertEquals("Bob", persons.get(0).getName());
	Assert.assertEquals("Alice", persons.get(1).getName());
	Assert.assertEquals("Charlie", persons.get(2).getName());
    }

    @Test
    public void shouldFilterUniqueElements()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4).stream();
	List<Integer> result = numbers.distinct().collect(toList());

	Assert.assertEquals(Arrays.asList(1, 2, 3, 4), result);
    }

    @Test
    public void shouldTruncateStream()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4).stream();
	List<Integer> result = numbers.limit(3).collect(toList());

	Assert.assertEquals(Arrays.asList(1, 2, 1), result);
    }

    @Test
    public void shouldSkipElements()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4).stream();
	List<Integer> result = numbers.skip(5).collect(toList());

	Assert.assertEquals(Arrays.asList(2, 4), result);
    }

    @Test
    public void shouldGetFirst2NumbersGreaterThan1Unique()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 2, 1, 3, 3, 2, 4).stream();
	List<Integer> result = numbers.filter(number -> number > 1).distinct().limit(2).collect(toList());

	Assert.assertEquals(Arrays.asList(2, 3), result);
    }

    /*
     * Mapping
     *
     * Intermediate Operators:
     * 		- map(Function<T,R> f)
     * 		- flatMap(Function<T,Stream<R>> f)
     */
    @Test
    public void shouldGetPersonsNames()
    {
	List<String> result = stream.map(person -> person.getName()).collect(toList());

	Assert.assertEquals(Arrays.asList("Alice", "Bob", "Charlie"), result);
    }

    @Test
    public void shouldGetPersonsNamesLength()
    {
	List<Integer> result = stream.map(person -> person.getName()).map(name -> name.length()).collect(toList());

	Assert.assertEquals(Arrays.asList(5, 3, 7), result);
    }

    @Test
    public void shouldGetPersonsHobbies()
    {
	List<List<String>> result = stream.map(person -> person.getHobbies()).collect(toList());

	List<String> aliceHobbies = Arrays.asList("Video Games", "Running");
	List<String> bobHobbies = Arrays.asList("Video Games");
	List<String> charlieHobbies = Arrays.asList("Video Games", "Running", "Cooking");
	Assert.assertEquals(Arrays.asList(aliceHobbies, bobHobbies, charlieHobbies), result);
    }

    @Test
    public void shouldGetPersonsHobbiesDistinct_Flat()
    {
	List<String> result = stream.flatMap(person -> person.getHobbies().stream()).distinct().collect(toList());

	Assert.assertEquals(Arrays.asList("Video Games", "Running", "Cooking"), result);
    }

    @Test
    public void shouldGetLettersDistinct()
    {
	Stream<String> words = Stream.of("Hello", "World");
	List<String> result = words.map(word -> word.split("")).flatMap(letter -> Arrays.stream(letter)).distinct().collect(toList());

	Assert.assertEquals(Arrays.asList("H", "e", "l", "o", "W", "r", "d"), result);
    }

    @Test
    public void shouldGetSquareOfNumbers()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5).stream();
	List<Integer> result = numbers.map(n -> n * n).collect(toList());

	Assert.assertEquals(Arrays.asList(1, 4, 9, 16, 25), result);
    }

    @Test
    public void shouldGetAllPairSums()
    {
	List<Integer> numbersI = Arrays.asList(1, 2, 3);
	List<Integer> numbersJ = Arrays.asList(3, 4);
	List<Object> result = numbersI.stream().flatMap(i -> numbersJ.stream().map(j -> i + j)).collect(toList());

	Assert.assertEquals(Arrays.asList(4, 5, 5, 6, 6, 7), result);

	// Option 1
	//List<Integer> intResult1 = numbersI.flatMapToInt(i -> numbersJ.mapToInt(j -> i+ j)).mapToObj(sum -> Integer.valueOf(sum)).collect(toList());
	//Assert.assertEquals(Arrays.asList(4, 5, 5, 6, 6, 7), intResult1);

	// Option 2
	//List<Integer> intResult2 = numbersI.flatMapToInt(i -> numbersJ.mapToInt(j -> i+ j)).boxed().collect(toList());
	//Assert.assertEquals(Arrays.asList(4, 5, 5, 6, 6, 7), intResult2);
    }

    /*
     * Finding and Matching
     *
     * Terminal Operators:
     * 		- boolean allMatch(Predicate p)
     * 		- boolean anyMatch(Predicate p)
     * 		- boolean noneMatch(Predicate p)
     * 		- Optional findFirst()
     * 		- Optional findAny()
     */
    @Test
    public void shouldAllLikeVideoGames()
    {
	boolean result = stream.allMatch(person -> person.getHobbies().contains("Video Games"));

	Assert.assertTrue(result);
    }

    @Test
    public void shouldNotBeAllMale()
    {
	boolean result = stream.allMatch(person -> person.isMale());

	Assert.assertFalse(result);
    }

    @Test
    public void shouldFindAtLeastOneFemale()
    {
	boolean result = stream.anyMatch(person -> !person.isMale());

	Assert.assertTrue(result);
    }

    @Test
    public void shouldNotFindAnyArchitects()
    {
	boolean result = stream.anyMatch(person -> person.getRole().equals(Role.ARCHITECT));

	Assert.assertFalse(result);
    }

    @Test
    public void shouldNotFindAnyArchitects_OtherImplementation()
    {
	boolean result = stream.noneMatch(person -> person.getRole().equals(Role.ARCHITECT));

	Assert.assertTrue(result);
    }

    /*
     * Optional (in a nutshell): container to represent the existence or absence or a value.
     *
     * 	- No NullPointerException
     * 	- Forced to check if value is present
     *
     *	- Useful Methods
     * 		- T get() // returns value or throws NoSuchElementException if not present
     * 		- boolean isPresent() // return true if value is present; false otherwise
     * 		- void ifPresent(Consumer block) // executes 'block' if value is present
     * 		- T orElse(T other) // return value if present; return 'other' otherwise
     */
    @Test
    public void shouldFindSomeRandomMale()
    {
	Optional<Person> result = stream.filter(Person::isMale).findAny();

	Assert.assertTrue(result.get().isMale());
    }

    @Test
    public void shouldNotFindAnybodyOver50()
    {
	Optional<Person> result = stream.filter(person -> person.getAge() > 50).findAny();

	Assert.assertFalse(result.isPresent());
    }

    @Test
    public void shouldFindFirstMale()
    {
	Optional<Person> result = stream.filter(Person::isMale).findFirst();

	Assert.assertEquals("Bob", result.get().getName());
    }

    /*
     * Reducing: Fold all of the elements in the stream into a single result.
     *
     * Terminal Operators:
     * 		- Optional<T> reduce(BinaryOperator<T> accumulator)
     *		- T reduce(T identity, BinaryOperator<T> accumulator)
     */
    @Test
    public void shouldSumAllNumbers()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5).stream();
	//Integer sum = numbers.reduce(0, (a, b) -> a + b);
	Integer sum = numbers.reduce(0, Integer::sum);

	Assert.assertEquals(Integer.valueOf(15), sum);
    }

    @Test
    public void shouldFindMinimumAndMaximum()
    {
	List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
	//Optional<Integer> min = numbers.stream().reduce((a, b) -> a < b ? a : b);
	//Optional<Integer> max = numbers.stream().reduce((a, b) -> a > b ? a : b);
	Optional<Integer> min = numbers.stream().reduce(Integer::min);
	Optional<Integer> max = numbers.stream().reduce(Integer::max);

	Assert.assertEquals(Integer.valueOf(1), min.get());
	Assert.assertEquals(Integer.valueOf(5), max.get());
    }

    /*
     * Numeric Streams
     *
     * Intermediate Operators:
     * 		- IntStream mapToInt(ToIntFunction mapper)
     * 		- LongStream mapToLong(ToLongFunction mapper)
     * 		- DoubleStream mapTpDouble(ToDoubleFunction mapper)
     *
     * Terminal Operators:
     * 		- sum()
     * 		- min()
     * 		- max()
     *
     * Creating:
     * 		- IntStream.rangeClosed
     * 		- Stream.generate(Supplier s)
     * 		- Stream.iterate(T seed, UnaryOperator<T> f)
     */
    @Test
    public void shouldSumAllAges()
    {
	int sum = stream.mapToInt(person -> person.getAge()).sum();

	Assert.assertEquals(80, sum);
    }

    @Test
    public void shouldFindMinAge_Long()
    {
	OptionalLong min = stream.mapToLong(person -> person.getAge()).min();

	Assert.assertEquals(12, min.getAsLong());
    }

    @Test
    public void shouldFindMaxAge_Double()
    {
	OptionalDouble max = stream.mapToDouble(person -> person.getAge()).max();

	Assert.assertEquals(Double.valueOf(37.0), Double.valueOf(max.getAsDouble()));
    }

    @Test
    public void shouldCreateBoundedNumberStream_1To100Inclusive()
    {
	IntStream numbers = IntStream.rangeClosed(0, 100);

	Assert.assertEquals(5050, numbers.sum());
    }

    @Test
    public void shouldCreateBoundedNumberStream_1To100Exclusive()
    {
	IntStream numbers = IntStream.range(0, 100);

	Assert.assertEquals(4950, numbers.sum());
    }

    @Test
    public void shouldGenerateInfiniteRandomNumberSequence()
    {
	Stream<Double> numbers = Stream.generate(Math::random);

	numbers.limit(1000).forEach(number -> Assert.assertTrue(number > 0 && number < 1));
    }

    @Test
    public void shouldGenerateInfiniteSequenceOfZeroes()
    {
	Stream<Integer> numbers = Stream.iterate(0, n -> n);

	numbers.limit(1000).forEach(number -> Assert.assertTrue(number == 0));
    }

    @Test
    public void shouldGenerateInfiniteSequenceNaturalNumbers() // 1, 2, 3, 4...
    {
	Stream<Integer> numbers = Stream.iterate(1, n -> n + 1);

	Assert.assertEquals(5050, numbers.limit(100).mapToInt(n -> n.intValue()).sum());
    }

    /*
     * Grouping and Partitioning
     *
     * Terminal Operators:
     * 		- collect(Collector<T,A,R> collector)
     *
     * Collectors (in a nutshell): recipe indicating how to assemble the elements in the stream
     * 		- Assemble into a List
     * 			-> Collectors.toList()
     * 		- Assemble into a Map where the keys are similarities and the values are the elements
     * 			-> Collectors.groupingBy()
     * 		- Assemble into a Map where the keys are TRUE/FALSE and the values are the elements
     * 			-> Collectors.partitionBy()
     */
    @Test
    public void shouldPartitionByMales()
    {
	Map<Boolean, List<Person>> result = stream.collect(Collectors.partitioningBy(Person::isMale));

	Assert.assertEquals(1, result.get(Boolean.FALSE).size());
	Assert.assertEquals(2, result.get(Boolean.TRUE).size());
	Assert.assertEquals("Alice", result.get(Boolean.FALSE).get(0).getName());
    }

    @Test
    public void shouldGroupByRoles()
    {
	Map<Role, List<Person>> result = stream.collect(Collectors.groupingBy(person -> person.getRole()));

	Assert.assertEquals(1, result.get(Role.TESTER).size());
	Assert.assertEquals(2, result.get(Role.PROGRAMMER).size());
	Assert.assertEquals("Charlie", result.get(Role.TESTER).get(0).getName());
    }

    /*
     * Downstream Collectors: chain of collectors that allow for more advanced reduction
     *
     * Examples: Collectors.groupingBy(Function f, Collector downstreamCollector)
     */
    @Test
    public void shouldGroupByRolesAndSex()
    {
	Map<Role, Map<Boolean, List<Person>>> result = stream.collect(Collectors.groupingBy(Person::getRole, Collectors.groupingBy(Person::isMale)));

	Assert.assertEquals(1, result.get(Role.TESTER).size());
	Assert.assertEquals(2, result.get(Role.PROGRAMMER).size());
	Assert.assertEquals(1, result.get(Role.PROGRAMMER).get(Boolean.FALSE).size());
	Assert.assertEquals("Alice", result.get(Role.PROGRAMMER).get(Boolean.FALSE).iterator().next().getName());
	Assert.assertEquals(1, result.get(Role.PROGRAMMER).get(Boolean.TRUE).size());
	Assert.assertEquals("Bob", result.get(Role.PROGRAMMER).get(Boolean.TRUE).iterator().next().getName());
    }

    @Test
    public void shouldCountPersonsByRoles()
    {
	Map<Role, Long> result = stream.collect(Collectors.groupingBy(Person::getRole, Collectors.counting()));

	Assert.assertEquals(Long.valueOf(1), result.get(Role.TESTER));
	Assert.assertEquals(Long.valueOf(2), result.get(Role.PROGRAMMER));
    }
}