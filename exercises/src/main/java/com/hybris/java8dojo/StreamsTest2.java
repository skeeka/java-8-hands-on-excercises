package com.hybris.java8dojo;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalLong;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.xebia.domain.Person;
import com.xebia.domain.Person.Role;

public class StreamsTest2 {

    private Person alice;
    private Person bob;
    private Person charlie;
    private Stream<Person> stream;

    @Before
    public void setUp() throws Exception {
        alice = new Person("Alice", 31, Boolean.FALSE, Role.PROGRAMMER, "Video Games", "Running");
        bob = new Person("Bob", 12, Boolean.TRUE, Role.PROGRAMMER, "Video Games");
        charlie = new Person("Charlie", 37, Boolean.TRUE, Role.TESTER, "Video Games", "Running", "Cooking");
        stream = Stream.of(alice, bob, charlie);
    }

    /*
     * Filtering and Slicing
     *
     * Intermediate Operators:
     * 		- filter(Predicate p)
     * 		- sorted(Comparator c)
     * 		- distinct()
     * 		- limit(long n)
     * 		- skip(long n)
     */
    @Test
    public void shouldFilterByNameEquals()
    {
	List<Person> persons = null;

	Assert.assertEquals("Alice", persons.iterator().next().getName());
    }

    @Test
    public void shouldFilterByAgeGreaterThan()
    {
	List<Person> persons = null;

	Assert.assertEquals(2, persons.size());
	Assert.assertTrue(persons.stream().anyMatch(person -> "Alice".equals(person.getName())));
	Assert.assertTrue(persons.stream().anyMatch(person -> "Charlie".equals(person.getName())));
    }

    @Test
    public void shouldFilterByHobbie_Running()
    {
	List<Person> persons = null;

	Assert.assertEquals(2, persons.size());
	Assert.assertTrue(persons.stream().anyMatch(person -> "Alice".equals(person.getName())));
	Assert.assertTrue(persons.stream().anyMatch(person -> "Charlie".equals(person.getName())));
    }

    @Test
    public void shouldFilterBySexAndRole()
    {
	List<Person> persons = null;

	Assert.assertEquals(1, persons.size());
	Assert.assertTrue(persons.stream().anyMatch(person -> "Bob".equals(person.getName())));
    }

    @Test
    public void shouldSortByAge()
    {
	List<Person> persons = null;

	Assert.assertEquals("Bob", persons.get(0).getName());
	Assert.assertEquals("Alice", persons.get(1).getName());
	Assert.assertEquals("Charlie", persons.get(2).getName());
    }

    @Test
    public void shouldFilterUniqueElements()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4).stream();
	List<Integer> result = null;

	Assert.assertEquals(Arrays.asList(1, 2, 3, 4), result);
    }

    @Test
    public void shouldTruncateStream()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4).stream();
	List<Integer> result = null;

	Assert.assertEquals(Arrays.asList(1, 2, 1), result);
    }

    @Test
    public void shouldSkipElements()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 1, 3, 3, 2, 4).stream();
	List<Integer> result = null;

	Assert.assertEquals(Arrays.asList(2, 4), result);
    }

    @Test
    public void shouldGetFirst2NumbersGreaterThan1Unique()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 2, 1, 3, 3, 2, 4).stream();
	List<Integer> result = null;

	Assert.assertEquals(Arrays.asList(2, 3), result);
    }

    /*
     * Mapping
     *
     * Intermediate Operators:
     * 		- map(Function<T,R> f)
     * 		- flatMap(Function<T,Stream<R>> f)
     */
    @Test
    public void shouldGetPersonsNames()
    {
	List<String> result = null;

	Assert.assertEquals(Arrays.asList("Alice", "Bob", "Charlie"), result);
    }

    @Test
    public void shouldGetPersonsNamesLength()
    {
	List<Integer> result = null;

	Assert.assertEquals(Arrays.asList(5, 3, 7), result);
    }

    @Test
    public void shouldGetPersonsHobbies()
    {
	List<String> result = null;

	List<String> aliceHobbies = Arrays.asList("Video Games", "Running");
	List<String> bobHobbies = Arrays.asList("Video Games");
	List<String> charlieHobbies = Arrays.asList("Video Games", "Running", "Cooking");
	Assert.assertEquals(Arrays.asList(aliceHobbies, bobHobbies, charlieHobbies), result);
    }

    @Test
    public void shouldGetPersonsHobbiesDistinct_Flat()
    {
	List<String> result = null;

	Assert.assertEquals(Arrays.asList("Video Games", "Running", "Cooking"), result);
    }

    @Test
    public void shouldGetLettersDistinct()
    {
	Stream<String> words = Stream.of("Hello", "World");
	List<String> result = null;

	Assert.assertEquals(Arrays.asList("H", "e", "l", "o", "W", "r", "d"), result);
    }

    @Test
    public void shouldGetSquareOfNumbers()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5).stream();
	List<Integer> result = null;

	Assert.assertEquals(Arrays.asList(1, 4, 9, 16, 25), result);
    }

    @Test
    public void shouldGetAllPairSums() // [(1+3), (1+4), (2+3), (2+4), (3+3), (3+4)]
    {
	List<Integer> numbersI = Arrays.asList(1, 2, 3);
	List<Integer> numbersJ = Arrays.asList(3, 4);
	List<Object> result = null;

	Assert.assertEquals(Arrays.asList(4, 5, 5, 6, 6, 7), result);
    }

    /*
     * Finding and Matching
     *
     * Terminal Operators:
     * 		- boolean allMatch(Predicate p)
     * 		- boolean anyMatch(Predicate p)
     * 		- boolean noneMatch(Predicate p)
     * 		- Optional findFirst()
     * 		- Optional findAny()
     */
    @Test
    public void shouldAllLikeVideoGames()
    {
	boolean result = Boolean.FALSE;

	Assert.assertTrue(result);
    }

    @Test
    public void shouldNotBeAllMale()
    {
	boolean result = Boolean.TRUE;

	Assert.assertFalse(result);
    }

    @Test
    public void shouldFindAtLeastOneFemale()
    {
	boolean result = Boolean.FALSE;

	Assert.assertTrue(result);
    }

    @Test
    public void shouldNotFindAnyArchitects()
    {
	boolean result = Boolean.TRUE;

	Assert.assertFalse(result);
    }

    @Test
    public void shouldNotFindAnyArchitects_OtherImplementation()
    {
	boolean result = Boolean.FALSE;

	Assert.assertTrue(result);
    }

    /*
     * Optional (in a nutshell): container to represent the existence or absence or a value.
     *
     * 	- No NullPointerException
     * 	- Forced to check if value is present
     *
     *	- Useful Methods
     * 		- T get() // returns value or throws NoSuchElementException if not present
     * 		- boolean isPresent() // return true if value is present; false otherwise
     * 		- void ifPresent(Consumer block) // executes 'block' if value is present
     * 		- T orElse(T other) // return value if present; return 'other' otherwise
     */
    @Test
    public void shouldFindSomeRandomMale()
    {
	Optional<Person> result = null;

	Assert.assertTrue(result.get().isMale());
    }

    @Test
    public void shouldNotFindAnybodyOver50()
    {
	Optional<Person> result = null;

	Assert.assertFalse(result.isPresent());
    }

    @Test
    public void shouldFindFirstMale()
    {
	Optional<Person> result = null;

	Assert.assertEquals("Bob", result.get().getName());
    }

    /*
     * Reducing: Fold all of the elements in the stream into a single result.
     *
     * Terminal Operators:
     * 		- Optional<T> reduce(BinaryOperator<T> accumulator)
     *		- T reduce(T identity, BinaryOperator<T> accumulator)
     */
    @Test
    public void shouldSumAllNumbers()
    {
	Stream<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5).stream();
	Integer sum = null;

	Assert.assertEquals(Integer.valueOf(15), sum);
    }

    @Test
    public void shouldFindMinimumAndMaximum()
    {
	List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
	Optional<Integer> min = null;
	Optional<Integer> max = null;

	Assert.assertEquals(Integer.valueOf(1), min.get());
	Assert.assertEquals(Integer.valueOf(5), max.get());
    }

    /*
     * Numeric Streams
     *
     * Intermediate Operators:
     * 		- IntStream mapToInt(ToIntFunction mapper)
     * 		- LongStream mapToLong(ToLongFunction mapper)
     * 		- DoubleStream mapTpDouble(ToDoubleFunction mapper)
     *
     * Terminal Operators:
     * 		- sum()
     * 		- min()
     * 		- max()
     *
     * Creating:
     * 		- IntStream.range
     * 		- IntStream.rangeClosed
     * 		- Stream.generate(Supplier s)
     * 		- Stream.iterate(T seed, UnaryOperator<T> f)
     */
    @Test
    public void shouldSumAllAges()
    {
	int sum = 0;

	Assert.assertEquals(80, sum);
    }

    @Test
    public void shouldFindMinAge_Long()
    {
	OptionalLong min = OptionalLong.of(0);

	Assert.assertEquals(12, min.getAsLong());
    }

    @Test
    public void shouldFindMaxAge_Double()
    {
	OptionalDouble max = OptionalDouble.of(0);

	Assert.assertEquals(Double.valueOf(37.0), Double.valueOf(max.getAsDouble()));
    }

    @Test
    public void shouldCreateBoundedNumberStream_1To100Inclusive() // [1, 100]
    {
	IntStream numbers = null;

	Assert.assertEquals(5050, numbers.sum());
    }

    @Test
    public void shouldCreateBoundedNumberStream_1To100Exclusive() // [1, 100[
    {
	IntStream numbers = null;

	Assert.assertEquals(4950, numbers.sum());
    }

    @Test
    public void shouldGenerateInfiniteRandomNumberSequence()
    {
	Stream<Double> numbers = null;

	numbers.limit(1000).forEach(number -> Assert.assertTrue(number > 0 && number < 1));
    }

    @Test
    public void shouldGenerateInfiniteSequenceOfZeroes() // 0, 0, 0, 0...
    {
	Stream<Integer> numbers = null;

	numbers.limit(1000).forEach(number -> Assert.assertTrue(number == 0));
    }

    @Test
    public void shouldGenerateInfiniteSequenceNaturalNumbers() // 1, 2, 3, 4...
    {
	Stream<Integer> numbers = null;

	Assert.assertEquals(5050, numbers.limit(100).mapToInt(n -> n.intValue()).sum());
    }

    /*
     * Grouping and Partitioning
     *
     * Terminal Operators:
     * 		- collect(Collector<T,A,R> collector)
     *
     * Collectors (in a nutshell): recipe indicating how to assemble the elements in the stream
     * 		- Assemble into a List
     * 			-> Collectors.toList()
     * 		- Assemble into a Map where the keys are similarities and the values are the elements
     * 			-> Collectors.groupingBy()
     * 		- Assemble into a Map where the keys are TRUE/FALSE and the values are the elements
     * 			-> Collectors.partitionBy()
     */
    @Test
    public void shouldPartitionByMales()
    {
	Map<Boolean, List<Person>> result = null;

	Assert.assertEquals(1, result.get(Boolean.FALSE).size());
	Assert.assertEquals(2, result.get(Boolean.TRUE).size());
	Assert.assertEquals("Alice", result.get(Boolean.FALSE).get(0).getName());
    }

    @Test
    public void shouldGroupByRoles()
    {
	Map<Role, List<Person>> result = null;

	Assert.assertEquals(1, result.get(Role.TESTER).size());
	Assert.assertEquals(2, result.get(Role.PROGRAMMER).size());
	Assert.assertEquals("Charlie", result.get(Role.TESTER).get(0).getName());
    }

    /*
     * Downstream Collectors: chain of collectors that allow for more advanced reduction
     *
     * Examples: Collectors.groupingBy(Function f, Collector downstreamCollector)
     */
    @Test
    public void shouldGroupByRolesAndSex()
    {
	Map<Role, Map<Boolean, List<Person>>> result = null;

	Assert.assertEquals(1, result.get(Role.TESTER).size());
	Assert.assertEquals(2, result.get(Role.PROGRAMMER).size());
	Assert.assertEquals(1, result.get(Role.PROGRAMMER).get(Boolean.FALSE).size());
	Assert.assertEquals("Alice", result.get(Role.PROGRAMMER).get(Boolean.FALSE).iterator().next().getName());
	Assert.assertEquals(1, result.get(Role.PROGRAMMER).get(Boolean.TRUE).size());
	Assert.assertEquals("Bob", result.get(Role.PROGRAMMER).get(Boolean.TRUE).iterator().next().getName());
    }

    @Test
    public void shouldCountPersonsByRoles()
    {
	Map<Role, Long> result = null;

	Assert.assertEquals(Long.valueOf(1), result.get(Role.TESTER));
	Assert.assertEquals(Long.valueOf(2), result.get(Role.PROGRAMMER));
    }
}