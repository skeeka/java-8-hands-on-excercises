package com.hybris.java8dojo;

/**
 * Created by i840081 on 2015-07-07.
 */
@FunctionalInterface
public interface KangarooJumpInterface
{
    Integer calculateJumpForce();
}
