package com.hybris.java8dojo;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Created by i840081 on 2015-07-06.
 */
public class MethodReferencesTest {

    /*
        You use lambda expressions to create anonymous methods.
        Sometimes, however, a lambda expression does nothing but call an existing method.
        In those cases, it's often clearer to refer to the existing method by name.

     */

    public class Kangaroo
    {

        private Integer jumpForce;

        public Integer getJumpForce() {
            return jumpForce;
        }

        public void setJumpForce(KangarooJumpInterface jumpForce) {
            this.jumpForce = jumpForce.calculateJumpForce();
        }

    }
    
    public class KangarooJumpCalc implements KangarooJumpInterface
    {
        @Override
        public Integer calculateJumpForce() {
            return 10;
        }
    }

    @Test
    public void testKangarooJumpForce()
    {
        Kangaroo kangaroo = new Kangaroo();
        kangaroo.setJumpForce(()->10);

        assertThat(kangaroo.getJumpForce(), is(10));
    }

    //instance reference
    @Test
    public void testKangarooJumpForceCooler()
    {
        Kangaroo kangaroo = new Kangaroo();
        KangarooJumpCalc kangarooJumpCalc = new KangarooJumpCalc();
        //kangaroo.setJumpForce(() -> kangarooJumpCalc.calculateJumpForce());
        kangaroo.setJumpForce(kangarooJumpCalc::calculateJumpForce);

        assertThat(kangaroo.getJumpForce(), is(10));
    }

    //instance reference
    @Test
    public void sortKangaroosMethodReference()
    {
        List<String> mob = asList("wallaby", "red", "grey", "boomer", "pademelon");

        //1
        Collections.sort(mob, new Comparator<String>() {
            @Override
            public int compare(String s, String anotherString) {
                return s.compareTo(anotherString);
            }
        });

        //2
        Collections.sort(mob, (s, anotherString) -> s.compareTo(anotherString));

        //3
        Collections.sort(mob, String::compareTo);

        assertThat(mob, is(asList("boomer", "grey", "pademelon", "red", "wallaby")));
    }

}
