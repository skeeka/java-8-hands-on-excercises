package com.xebia.java8_1.lambdas;

import com.xebia.domain.Person;
import com.xebia.domain.Persons;

public class LambdaLabs {

    public static class PersonSelector {

        public static boolean maleAdults(Person person) {
            return person.isAdult() && person.isMale();
        }

        public static boolean femaleAdults(Person person) {
            return !maleAdults(person);
        }
    }

    public static Persons filterUsingAnonymousInnerClass(Persons persons) {
        return persons;
    }

    public static Persons filterWithLambda(Persons persons) {
        return persons;
    }

    public static Persons filterWithMethodReference(Persons persons) {
        return persons;
    }

    public static Persons filterMaleAdultsWithStaticMethodReference(Persons persons) {
        return persons;
    }

    public static Persons sortPersonsWithLambda(Persons persons) {
        return persons;
    }

    public static Persons sortWithComparing(Persons persons) {
        return persons;
    }

}
